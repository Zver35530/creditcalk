public class MonthlyPayment {

    int paymentNumber;
    double percentPayment, bodyPayment;

    public MonthlyPayment(int paymentNumber, double percentPayment, double bodyPayment) {
        this.paymentNumber = paymentNumber;
        this.percentPayment = percentPayment;
        this.bodyPayment = bodyPayment;
    }

    @Override
    public String toString() {
        return "номер платежа " + paymentNumber + " , Начисленные проценты = " + percentPayment + " , Основной долг " + bodyPayment;
    }
}
