public class Credit {

    double percent, summa, percent_mes;
    int srok_mes;
    MonthlyPayment[] monthlyPayments;

    public Credit(double percent, double summa, int srok_mes) {
        this.percent = percent/100;
        this.summa = summa;
        this.srok_mes = srok_mes;

        percent_mes = this.percent/12;

        monthlyPayments = new MonthlyPayment[srok_mes];

        double curSumma = this.summa;
        for (int iMonth = 0; iMonth < srok_mes; iMonth++){
            monthlyPayments[iMonth] = new MonthlyPayment(
                    iMonth,
                    curSumma*percent_mes,
                    getAnnualPayment() - curSumma*percent_mes );

            curSumma -= getAnnualPayment() - curSumma*percent_mes;
        }
    }

    public double getAnnualPayment(){
        return summa * percent_mes * Math.pow (1 + percent_mes, srok_mes)/( Math.pow ( 1 + percent_mes, srok_mes ) - 1);
    }

    public double getPereplata(){
        return getAnnualPayment()*srok_mes - summa;
    }
    @Override
    public String toString() {
        return "сумма = " + summa + ", с процентом = " + percent + ", на " + srok_mes + " месяцев"
                + ", платёж составит: " + getAnnualPayment();
    }
}
